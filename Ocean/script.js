// once the page has finished loading - run this
$(document).ready(function () {
    $(document).on('click', '#lineStatus', function () {
        // empty the contents in the elements
        $('.name').empty();
        $('.status').empty();
        $('.error').empty();
        // fetching value and assign it to a variable
        var stationName = $('#searchEntry').val();
        lineInfo(stationName);
    });
});
// once the page has finished loading - run this
$(document).ready(function () {
    $(document).on('click', '#routeCheck', function () {
        // empty the contents in the elements
        $('.pricing').empty();
        $('.duration').empty();
        $('.step').empty();
        $('.detail').empty();
        $('.journeyError').empty();
        // fetching values and assinging it to variables
        var start = $('#start').val();
        var end = $('#end').val();
        routeChecker(start, end);
    });
});

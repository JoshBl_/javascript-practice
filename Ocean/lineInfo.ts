function lineInfo(tubeName: string | number | string[]) {
    console.log("Getting line status for " + tubeName + "!");
    // get request through Ajax
    $.ajax({
        url: "https://api.tfl.gov.uk/line/mode/tube/status"
    }).done(function (data) {
        // boolean declared - used for checking if the tube status was found
        let findCheck = false;
        // iterates through the response for each station
        data.forEach(station => {
            // if the name field matches the name specified - run following 
            if (station.name === tubeName) {
                // log to console
                console.log('Current status on ' + tubeName + ' is ' + station.lineStatuses[0].statusSeverityDescription);
                // new variable which holds the line status
                let stationStatus = station.lineStatuses[0].statusSeverityDescription;
                // display the line name and the status onto the screen
                $('.name').append('<p>Line name: ' + tubeName + '</p>');
                $('.status').append('<p>Line status: ' + stationStatus + '</p>');
                // change boolean value to true
                findCheck = true;
            }
        });
        // if findCheck is false...
        if (findCheck === false) {
            // print an error to the user
            $('.error').append('<p>Error! Invalid name!');
            // log to console
            console.log('Error! Invalid name!');
        };
    }).fail(function () {
        // if the GET request fails, print the following to the screen and console
        console.log("Error! Can't retrieve Underground status!");
        $('.error').append("<p>Network Error! Can't retrieve Underground status!</p>");
    });
}

//you can give multiple things for console.log to print
//separate each thing with a comma
console.log("Name:", 'Joshua Blewitt');
console.log("Age", 26);
console.log("Feet Tall:", Math.round(74/12));
console.log("And Inches:",74 - (Math.round(74/12)*12));
console.log("Age * Height:",43*74);
console.log("Age * Feet:",43 * Math.round(74/12));
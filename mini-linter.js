let story = 'Last weekend, I took literally the most beautiful bike ride of my life. The route is called "The 9W to Nyack" and it actually stretches all the way from Riverside Park in Manhattan to South Nyack, New Jersey. It\'s really an adventure from beginning to end! It is a 48 mile loop and it basically took me an entire day. I stopped at Riverbank State Park to take some extremely artsy photos. It was a short stop, though, because I had a really long way left to go. After a quick photo op at the very popular Little Red Lighthouse, I began my trek across the George Washington Bridge into New Jersey.  The GW is actually very long - 4,760 feet! I was already very tired by the time I got to the other side.  An hour later, I reached Greenbrook Nature Sanctuary, an extremely beautiful park along the coast of the Hudson.  Something that was very surprising to me was that near the end of the route you actually cross back into New York! At this point, you are very close to the end.';

let overusedWords = ['really', 'very', 'basically'];

let unnecessaryWords = ['extremely', 'literally', 'actually' ];

//create new array where each element is split when there is a space - ' '
let storyWords = story.split(' ')

//using negation (the ! character) to find words that are not equal to the unnecessaryWords array. Return the result to the betterWords array
let betterWords = storyWords.filter(function(word) {
  return !unnecessaryWords.includes(word)
})

//similar to the above, but using the overusedWords array
betterWords = betterWords.filter(function(word) {
	return !overusedWords.includes(word)
})

//new variable defined
let sentencesCount = 0
for (word of betterWords)
  {
    if(word[word.length-1] === '.' || word[word.length-1] === '!')
      {
        sentencesCount+=1
      }
  }

//new variables to count use of overused words
let reallyCount = 0
let literallyCount = 0
let actuallyCount = 0

//count each time an overused word is used
for (word of storyWords)
  {
    if(word === 'really')
      {
        reallyCount += 1
      }
    else if (word === 'literally')
      {
        literallyCount += 1
      }
    else if (word === 'actually')
      {
        actuallyCount += 1
      }
  }

//print out the array but using the join method to separate each element
console.log(betterWords.join(' '))

console.log('Word count ' + betterWords.length)
console.log('Sentence count ' + sentencesCount)
console.log('Overused word count - "really" ' + reallyCount)
console.log('Overused word count - "literally" ' + literallyCount)
console.log('Overused word count - "actually" ' + actuallyCount)
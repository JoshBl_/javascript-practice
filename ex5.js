//const variable = a variable that will never change!
const invincible = 94;
let changeme = 82;

console.log(`invincible=${invincible} but changeme=${changeme}`);

//this will work just fine
changeme = 100;

//comment out the code below to see how you can't change the const variable
//invincible = 100000;

console.log(`After change: invincible=${invincible} but changeme=${changeme}`);
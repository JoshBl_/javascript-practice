// used in london.html
// this will list all children elements in the Document Object Model
function listDomElements() {
    var children = document.body.childNodes;
    var i;

    for (i = 0; i < children.length; i++) {
        console.log(children[i]);
    }
}

// an easier way of interacting with DOM is by retrieving elements by id
var description = document.getElementById('description');
console.log(description.innerHTML);

// creating new elements
// 1) create an element
// document.createElement('<tagName>');

// 2) create a text node
// document.createTextNode('<text>');

// 3) add children to element
// document.appendChild('<node>');

// new object called London with properties, inner objects and methods
var london = {
    name: 'London',
    population: 8308369,
    tallestBuilding: {
        name: 'Shard',
        height: '310m'
    },
    numberOfUniversities: 43,
    averageRent: 1106,
    dailyTubePassengerJourney: 3500000,
    olympics: [1908, 1948, 2012],
    updatePopulation: function (newPopulation) {
        this.population = newPopulation;
    }
};

function displayPopulation() {
    // making a new <p> for population (not yet attached to DOM)
    var population = document.createElement('p');

    // make some text content to put into <p>
    var content = document.createTextNode('Population: ' + london.population);

    // put the text content into the <p>
    population.appendChild(content);

    // population can be appended to the body and become visibile in the browser
    document.body.appendChild(population);
}

function displayTallestBuilding() {
    var building = document.createElement('p');
    var content = document.createTextNode('Tallest building name: ' + london.tallestBuilding.name + '\nTallest building height: ' + london.tallestBuilding.height);
    building.appendChild(content);
    document.body.appendChild(building);
}

function displayUniversities() {
    var universities = document.createElement('p');
    var content = document.createTextNode('Number of Universities: ' + london.numberOfUniversities);
    universities.appendChild(content);
    document.body.appendChild(universities);
}

function displayAverageRent() {
    var rent = document.createElement('p');
    var content = document.createTextNode('Average rent: ' + london.averageRent);
    rent.appendChild(content);
    document.body.appendChild(rent);
}

function displayTubePassengers() {
    var passengers = document.createElement('p');
    var content = document.createTextNode('Number of daily tube passengers: ' + london.dailyTubePassengerJourney)
    passengers.appendChild(content);
    document.body.appendChild(passengers);
}

function displayOlympics() {
    var olympics = document.createElement('p');
    var content = document.createTextNode('Years London held Olympics: ' + london.olympics);
    olympics.appendChild(content);
    document.body.appendChild(olympics);
}

function main() {
    displayPopulation();
    displayTallestBuilding();
    displayUniversities();
    displayAverageRent();
    displayTubePassengers();
    displayOlympics();
}
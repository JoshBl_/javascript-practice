// used in lesson2.html
var codebarIsAwesome = true;
var weatherIsAmazing = false;

console.log('Is codebar AWESOME? ' + codebarIsAwesome);
console.log('Is the weather in London amazing? ' + weatherIsAmazing);

var x = 6;
var y = 3;
var addition = x + y;

console.log('Addition: x + y = ' + addition);

var subtraction = x - y;

console.log('Subtraction: x - y = ' + subtraction);

var multiplication = x * y;

console.log('Multiplication: x * y = ' + multiplication);

var division = x / y;

console.log('Division: x / y = ' + division);

var a = 5;
var b = 3;

var modulus = a % b;

console.log('Remainder: a % b = ' + modulus);

var exponentiation = a ** b;

console.log('Exponentiation: a ** b = ' + exponentiation);

var increment = a++;
increment = a;

console.log('Increment: a++ = ' + increment);

var decrement = b--;
decrement = b;

console.log('Decrement: b-- = ' + decrement);

var apples = 'apples';
var oranges = 'oranges';

// compares two values - returns true if they are equal and false if they are not
var equal = apples === oranges;

console.log('Apples and oranges are the same: ' + equal);

var notEqual = apples !== oranges;

console.log('Apples and oranges are different: ' + notEqual);

var coaches = 20;
var students = 24;
var pizzas = 25;

var moreStudents = students > coaches;
var lessStudents = students < pizzas;

console.log('Are there more students than coaches? ' + moreStudents);
console.log('Are there fewer students than pizzas? ' + lessStudents);

//combining operators
var enoughPizzas = (coaches + students) < pizzas;
console.log('Do we have enough pizzas for everbody? ' + enoughPizzas);

var totalPeople = students + coaches;

// if statements
if (totalPeople > pizzas) {
    console.log('We have more people than pizzas!');
} else {
    console.log("We have far too much pizza, which isn't a bad thing!");
}

// while loop
var i = 1;
var total = 0;

while (i < 10) {
    console.log('Current number is ' + i);
    console.log('Adding to total');
    total = total + i;
    i++
}

console.log('Total is ' + total);

// for loops - commonly used with arrays!

/*
for(before the loop starts; test before each iteration; after each iteration) {
    set of statements
}
*/

var number;
var newTotal = 0;

for (number = 1; number <= 10; number++) {
    console.log('Number value is - ' + number);
}


// arrays - a data structure
var animals = ['dog', 'cat', 'rabbit', 'horse', 'elephant', 'monkey'];

// retrieving items from an array uses square bracket notation
console.log(animals[0]);
console.log(animals[1]);
console.log(animals[2]);

// we can use the length property to return the size of the array
var c;

for (c = 0; c < animals.length; c++) {
    var animal = animals[c];
    console.log(animal);
}

// methods normally mean that a function belong to an object - common to see them used with the dot notation

// adding objects to an array

// this adds an element to the beginning of the array
animals.unshift('cow');

// this adds an element to the end of the array
animals.push('zebra');

console.log(animals);

// pop removes and returns the last element
console.log(animals.pop());

// shift removes and returns the first element
console.log(animals.shift());

// to order elements of an array - use sort()
var names = ['Joshua', 'Bill', 'Cassandra', 'Liam', 'Sam', 'Hannah']

names.sort();
console.log(names);

names.sort().reverse();
console.log(names);

// sort can be (and usually) customised to sort things in any way you like It can take a function as an argument to tell it what to do

// sort passes pairs of entries to the function, if this returns a number less than zero, then sort knows that a should come before b, if the number is greater than zero then b should come before a
function sortNumbersAscending(a, b) {
    return a - b;
}

// if this function returns a number greater than zero, then sort knows b comes before a, if the number if less than zero then a should come before b
function sortNumbersDescending(a, b) {
    return b - a;
}

var nums = [ 1, 5, 3, 19, 2, 10];

nums.sort(sortNumbersAscending);

console.log(nums);

nums.sort(sortNumbersDescending);

console.log(nums);

// new array with elements
var fruitAndVeg = ['apple', 'orange', 'banana', 'kiwi', 'avocado', 'celergy', 'aubergine'];
// new blank array
var noAvocados = [];

// for loop that will loop as long as 'n' is less than the length of the array, increments n by 1 every time the loop ends
for (var n = 0; n < fruitAndVeg.length; n++) {
    // use square bracket notation to access individual elements of the array and ensure the element doesn't equal 'avocado'
    if (fruitAndVeg[n] !== 'avocado') {
        // if returns true - take the element and add to the end of the array called noAvocados
        noAvocados.push(fruitAndVeg[n]);
    }
// if returns false - end loop
}

console.log(fruitAndVeg);
console.log(noAvocados);


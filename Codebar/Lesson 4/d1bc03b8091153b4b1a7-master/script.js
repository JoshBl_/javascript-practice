// github user finder example
$(document).ready(function () {
    $(document).on('keypress', '#username', function (event) {
        if (event.which === 13) { // check if the key press was enter
            $('#profile h2').empty();
            $('.avatar').empty();
            $('.information').empty();
            // using this keyword to find parent element
            var input = $(this);
            // retrieve value from input and assign it to variable
            var username = input.val();

            console.log('username was: ' + username);
            getGitHubInfo(username);
        }
    });
});

function getGitHubInfo(username) {
    // build the URL to perform GET request with
    //var url = 'https://api.github.com/users/' + username

    // performing GET request with ajax
    $.ajax({
        url: 'https://api.github.com/users/' + username
        //dataType: 'json',
    }).done(function (data) {
        console.log(data);
        console.log(data.login);
        showUser(data);
    }).fail(function () {
        console.log('Something went wrong!');
    });

    /*
    // new object for XMLHttpRequest
    var xmlhttp = new XMLHttpRequest();

    // open method called; verb, url and not asychronous (browser will wait for the call to finish before continuing)
    xmlhttp.open('GET', url, false);
    xmlhttp.send();

    showUser(xmlhttp);
    */
}

function showUser(data) {
    // variable stores the response from GET request
    //var json = xmlhttp.responseText;
    // decode the data stored in responseText - turn it into a native JS object
    //var user = JSON.parse(json);
    console.log(data);
    $('#profile h2').append('<h2>Username is: ' + data.login + '</h2>');
    $('.avatar').append('<img src=' + data.avatar_url + '></img>');
    $('.information').append('<h2>GitHub profile link: ' + data.url + '</h2>');
}
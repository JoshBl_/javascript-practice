/* Exercise 2: Color picker */
// sets the preview class background to the colour in the text box
function setPreviewColor(color) {
    // clears the previous text
    $('.color-code').empty()
    // using the css method to set a css property
    $('.preview').css('background-color', color);
    var rgb = $('.preview').css('background-color');
    $('.color-code').append('<p>' + rgb + '</p>');
};

// adding colour to favourites list
function addBox(color) {
    $('#colors').prepend('<div class="item" style="background-color: ' + color + ';"></div>');
    $('#colors').focus();
}

// whenever the user finishes releasing a key from the keyboard - run this
$(document).keyup(function () {
    // assign current value in text box to variable
    var color = $('#color').val();
    // call method
    setPreviewColor(color);
});

// whenever the user presses the add to favourites button - a new box is added
$(document).on('click', '#add-to-favorite', function () {
    colorLength = $('#colors .item').length
    if (colorLength >= 16) {
        $('.item').last().remove();
    }
    // variable that holds the current text
    var color = $('#color').val();
    // calls method
    addBox(color);
    // resets the text field
    $('#color').val('');
})

// runs when the page is loaded
$(document).ready(function () {
    // new array
    var colors = ['22ac5e', 'd68236', '770077'];
    // for each item in the array, call the addBox method
    colors.forEach(function (color) {
        addBox(color);
    })
    // picks a random colour from the array, random generates a number between 0 and 1, then multiply it by the length of the arry and finally floor it to discard the fractional part of the number
    var number = Math.floor(Math.random() * colors.length);
    setPreviewColor(colors[number]);

    // stores the original colour
    var previewColor;

    // when the mouse cursor enters a box item run the following
    $(document).on('mouseenter', '.item', function () {
        // get the colour on the selected box
        previewColor = $('.preview').css('background-color');
        // change the background to that colour
        setPreviewColor($(this).css('background-color'));
    }).on('mouseleave', '.item', function () { // on leave change the background colour preview back to the original
        setPreviewColor(previewColor);
    })
})
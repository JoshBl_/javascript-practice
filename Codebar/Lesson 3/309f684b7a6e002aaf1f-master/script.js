/* Exercise 1: Wish list */
// variables for pending and completed items in the list
var pending;
var completed;

function addToTotal(pending, completed) {
    // fetch the number of completed and pending items
    pending = $('.pending').length;
    completed = $('.completed').length;
    // on the total class, display the following text
    $('.total').text('Pending: ' + pending + ' Completed: ' + completed);
}

function addToList(item) {
    // this returned a jQuery object that has selected the html element with id items. 
    $('#items').append('<li>' + item + '<span class="label pending">Pending</span>' + '</li>');
}

$(document).on('click', '#add-to-list', function () {
    // calls the addToList function but calls the current text in the text box to be added
    addToList($('#item').val());
    //clears the text in the text box
    $('#item').val("");
    // puts the cursor back in the text field after clicking the button
    $('#item').focus();
    addToTotal(pending, completed);
});

$(document).on('click', '.pending', function () {
    // 'this' is a special variable which gives the element that the event came from
    var li_node = $(this).parent();
    li_node.append("<span class='label success'>Done!</span>"); 
    li_node.addClass('completed');
    $(this).remove();
    addToTotal(pending, completed);
});

// prints a string to the console
// semicolon marks the end of a statement
console.log("Hello!");

var a = 5;
var b = 5;
// this variable uses an expression to combine two values (every expression can be reduced to a single value)
var c = a + b;

// function
function sayHello() {
    console.log("Hello!");
}

// function
function conversation() {
    // code between the braces is the body of the function
    sayHello();
    console.log("How are you?");
    console.log("See ya!");
}

// function with a parameter
function sayHelloPerson(person) {
    // return the value of this expression from the function - the value of the function call is the value that was given to return
    // if the function got to the end without ever seeing return then the value of the call will be undefined!
    return 'Hello ' + person + '!';
}

// function with multiple parameters
function newConversation(person, topic) {
    return "Hey " + person + ". Do you like " + topic + "?"
}

// another type of value is an object (which is a value)
// the object is referred to by the variable named person who has two properties
// you can access the properties by typing person.first_name or person.likes
// the '.' means to access an object's property - the value stored in the property of the object
// a property is like a variable, you can change the value using '='
var person_a = {
    first_name: "Joshua",
    likes: "Retro games",
    // a function in a object is called a method
    state : function() {
        // 'this' refers to the owner of the function (the person_a object owns the state function)
        // in other words this.first_name means the first_name property of this object
        return "Name: " + this.first_name + " Likes: " + this.likes
    }
};

person_b = person_a;


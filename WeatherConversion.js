//constant variable called kelvin - value of 293
const kelvin = 0;
console.log(kelvin);
//new variable which takes celsius and subtracts it by 273
const celsius = kelvin - 273;
console.log(celsius);
//calculates farenheit 
let farenheit = celsius * (9/5) + 32;
console.log(farenheit);
//we may get a decimal number, so floor it to a round number
farenheit = Math.floor(farenheit);
console.log(`The temperature is ${farenheit} degrees Fahrenheit`);

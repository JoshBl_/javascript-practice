let input = "a whale of a deal!"

const vowels = ['a', 'e', 'i', 'o', 'u']

let resultArray = []

//first for loop, performs while count is less than the input length
for(let count = 0; count < input.length; count++)
  {
    //second for loop, performs while vowelCount is less than the vowels array length
    for(let vowelCount = 0; vowelCount < vowels.length; vowelCount++)
      {
        //first if statement, performs if a letter from input is equal to the array element from vowels, continue
        if(input[count] === vowels[vowelCount])
          {
            //second if statement, performs if the letter from input is equal to 'e'
            if(input[count] === 'e')
              {
                  //push 'ee' to array
                resultArray.push('ee')
              }
            //similar to above, but only if the letter from input is equal to 'u'
            else if (input[count] === 'u')
              {
                  //push 'uu' to array
                resultArray.push('uu')
              }
            //if the letter doesn't match a vowel, do this action instead
            else
              {
                  //push the letter from input to the array
                resultArray.push(input[count])
              }
          }
        else
          {
            //do nothing!
          }
      }
  }
//print the array (joined and in upper case)
console.log(resultArray.join('').toUpperCase())
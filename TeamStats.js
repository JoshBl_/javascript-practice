//object
const team = {
    //arrays for players and games
    _players: [
      {
        firstName: 'Joe',
        lastname: 'Bloggs',
        age: 46,
      },
      {
        firstName: 'Bill',
        lastname: 'New',
        age: 25,
      },
      {
        firstName: 'Mike',
        lastname: 'Average',
        age: 30,
      }
    ],
    _games: [
      {
        opponent: 'Broncos',
        teamPoints: 42,
        opponentPoints: 27
      },
      {
        opponent: 'Cambridge Wizards',
        teamPoints: 10,
        opponentPoints: 58
      },
      {
        opponent: 'Milton Keynes Lions',
        teamPoints: 42,
        opponentPoints: 27
      },
    ],
    
    //getter methods for players and games keys
    //no setter methods because the values saved to the keys don't need to be changed
    get players() {return this._players},
    get games() {return this._games},
    
    //method to add players (three parameters)
    addPlayer (firstName, lastName, age) {
      //object for player
      let player = {
        firstName: firstName,
        lastName: lastName,
        age: age
      }
      //add player to players array
      this.players.push(player)
    },
    
    //method to add games (three parameters)
    addGame (opponentName, myTeamPoints, opponentPoints)
    {
      let game = {
        opponent: opponentName,
        teamPoints: myTeamPoints,
        opponentPoints: opponentPoints
      }
      this.games.push(game)
    }
  }
  
  console.log('Players')
  team.addPlayer('Josh', 'Blewitt', 26)
  team.addPlayer('David', 'Collins', 30)
  team.addPlayer('Toby', 'Walton', 26)
  
  console.log(team.players)
  console.log('\n')
  
  console.log('Games')
  team.addGame('Nottingham Tigers', 26, 20)
  team.addGame('London Dragons', 15, 27)
  team.addGame('Oxford Pandas', 15, 27)
  
  console.log(team.games)
const getUserChoice = (userInput) => {
    userInput = userInput.toLowerCase();
    if(userInput === 'rock' || userInput === 'scissors' || userInput === 'paper' || userInput === 'bomb')
      {
        return userInput
      }
    else
      {
        console.log('ERROR!')
      }
  }
  
  const getComputerChoice = () => {
    const number = Math.floor(Math.random() * 3)
    switch(number)
    {
      case 0:
          return 'rock'
      case 1:
          return 'paper'
      case 2:
          return 'scissors'
      case 3:
        return 'bomb'
    }
  }
  
  //console.log(getUserChoice('ROCK'))
  //console.log(getComputerChoice())
  
  const determineWinner = (getUserChoice, getComputerChoice) => {
    if (getUserChoice === getComputerChoice)
      {
        return 'It was a tie!'
      }
    if (getUserChoice === 'rock')
      {
        if (getComputerChoice === 'paper')
          {
            return 'Computer wins!'
          }
        else
          {
            return 'Player wins!'
          }
      }
    if (getUserChoice === 'paper')
      {
        if (getComputerChoice === 'scissors')
          {
            return 'Computer wins!'
          }
        else
          {
            return 'Player wins!'
          }
      }
    if (getUserChoice === 'scissors')
      {
        if (getComputerChoice === 'rock')
          {
            return 'Computer wins!'
          }
        else
          {
            return 'Player wins!'
          }
      }
    if(getUserChoice === 'bomb')
      {
        return 'Player wins - not matter what!'
      }
  }
  
  //console.log(determineWinner('rock', 'paper'))
  
  const playGame = () => {
    userChoice = getUserChoice('BOMB')
    computerChoice = getComputerChoice()
    console.log(userChoice)
    console.log(computerChoice)
    console.log(determineWinner(userChoice,computerChoice))
  }
  console.log(playGame())
//variables defined
const myAge = 26;
let earlyYears = 2;
console.log(myAge);
console.log(earlyYears);
//change value of earlyYears
earlyYears = earlyYears * 10.5;
console.log(earlyYears);
//set value of laterYears variable
let laterYears = myAge - 2;
console.log(laterYears);
laterYears *= 4;
console.log(laterYears);
//the value of myAgeInDogYears is earlyYears and laterYears added together
let myAgeInDogYears = earlyYears + laterYears;
//converting myName string to lowerCase using a built-in method
let myName = 'JOSH'.toLowerCase();
console.log(myName);
console.log(`My name is ${myName}. I am ${myAge} years old in human years which is ${myAgeInDogYears} years old in dog years.`)
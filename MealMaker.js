//object called menu
const menu = {
    //property with three arrays
    _courses : {
      appetizers : [],
      mains : [],
      desserts : []
    },
    
    //getter and setter methods for appetizers
    get appetizers() {},
    set appetizers(setApp) {},
    
    //getter and setter methods for mains
    get mains() {},
    set mains(setMain) {},
      
    //getter and setter methods for desserts
    get desserts() {},
    set desserts(setDes) {},
    
    //getter methods for courses property
    get courses() {
      //return key/value pairs for appetizers, mains and desserts
      return {appetizers : this.appetizers,
             mains : this.mains,
             desserts : this.desserts}
    },
    //method takes 3 arguments
    addDishToCourse(courseName, dishName, dishPrice)
    {
      //creates object called dish
      const dish = {
        //key value pairs
        name: dishName,
        price: dishPrice
      }
      //pushes object into array based on courseName value
      this._courses[courseName].push(dish)
    },
    //method to get a random dish
    getRandomDishFromCourse : function (courseName)
    {
      //retrieve array given course name and store in dishes variable
      const dishes = this._courses[courseName]
      //generate random index by multiply random by length of dishes array and rounded by using floor
      const randomIndex = Math.floor(Math.random() * dishes.length)
      return dishes[randomIndex]
    },
    //method to generate a random meal
    generateRandomMeal : function ()
    {
      //new variables for each course
      const appetizer = this.getRandomDishFromCourse('appetizers')
      const main = this.getRandomDishFromCourse('mains')
      const dessert = this.getRandomDishFromCourse('desserts')
      //calculate total cost
      const totalPrice = appetizer.price + main.price + dessert.price
      return `Your meal is ${appetizer.name}, ${main.name}, ${dessert.name}. The price is $${totalPrice}`
    }
  }
  //create random meals
  menu.addDishToCourse('appetizers', 'Duck Liver', 3.50)
  menu.addDishToCourse('mains', 'Bacon Cheese Burger', 10.50)
  menu.addDishToCourse('desserts', 'Stick Toffee Pudding', 7.50)
  menu.addDishToCourse('appetizers', 'Salad', 1.25)
  menu.addDishToCourse('mains', 'Steak', 8.50)
  menu.addDishToCourse('desserts', 'Ice Cream', 5.20)
  
  //generate a random meal
  let meal = menu.generateRandomMeal()
  console.log(meal)
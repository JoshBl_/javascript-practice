getSleepHours = (day) => {
    switch (day)
      {
        case 'monday':
          return 8
        case 'tuesday':
          return 8
        case 'wednesday':
          return 8
        case 'thursday':
          return 8
        case 'friday':
          return 8
        case 'saturday':
          return 8
        case 'sunday':
          return 8
      }
  }
  
  //console.log(getSleepHours('monday'))
  
  getActualSleepHours = () =>
    getSleepHours('monday') + getSleepHours('tuesday') + getSleepHours('wednesday') + 
    getSleepHours('thursday') + getSleepHours('friday') + getSleepHours('saturday') + getSleepHours('sunday');
  
  console.log(getActualSleepHours())
  
  getIdealSleepHours = () => {
    const idealHours = 9
    return idealHours * 7
    //7 for the days of the week
  }
  console.log(getIdealSleepHours())
  
  calculateSleepDebt = () => {
    const actualSleepHours = getActualSleepHours()
    const idealSleepHours = getIdealSleepHours()
    if(actualSleepHours === idealSleepHours)
      {
        console.log('Perfect sleep achieved!')
      }
    else if (actualSleepHours > idealSleepHours)
      {
        console.log('More sleep than needed achieved!')
        console.log(`Total hours additional sleep achieved - ${actualSleepHours - idealSleepHours}`)
      }
    else if (actualSleepHours < idealSleepHours) {
      console.log('Get some rest!')
      console.log(`Total additional hours of sleep needed - ${idealSleepHours - actualSleepHours}`)
    }
  }
  console.log(calculateSleepDebt())
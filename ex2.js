//this is a comment, everything after is ignored
console.log("I am code.");

/*this is also a comment. You can use this for
* longer things you want to say.
*/

console.log("So am I");

//the code inside the next comment will not run
/*
console.log("I will not run");
*/
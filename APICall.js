console.log("Let's call the API for money exchange!");

//create new variables, one for creating a new instance of XMLHttpRequest and the other for storing the URL
var request = new XMLHttpRequest();
var url = "https://api.exchangeratesapi.io/latest?base=GBP";

//request is part of XMLHttpRequest - open requires the method (GET in this case), the URL and the async boolean is set to false
request.open("GET", url, false);
//send initiates the request
request.send();
//prints the response to the console
console.log(request.response);





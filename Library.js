class Media {
    constructor (title, isCheckedOut, ratings) {
      this._title = title
      this._isCheckedOut = false
      this._ratings = []
    }
    
    get title() {return this._title}
    get isCheckedOut() {return this._isCheckedOut}
    get ratings() {return this._ratings}
    
    getAverageRating(){
      let ratingsSum = this.ratings.reduce((currentSum, rating) => currentSum + rating, 0)
      const arrayLength = this._ratings.length
      let averageRates = ratingsSum / arrayLength
      return `The average rating for this is ${averageRates}`
    }
    
    toggleCheckOutStatus(){
      if (this._isCheckedOut == false)
        {
          this._isCheckedOut = true
        }
      else
        {
          this._isCheckedOut = false
        }
    }
    
    addRating(newRating){
      this.ratings.push(newRating);
    }
  }
  
  class Book extends Media {
    constructor (title, author, pages)
    {
      super(title)
      this._author = author
      this._pages = pages
    }
    
    get author() {return this._author}
    get pages() {return this._pages}
  }
  
  class Movie extends Media {
    constructor (title, director, runTime)
    {
      super(title)
      this._director = director
      this._runTime = runTime
    }
    
    get director() {return this._director}
    get runTime() {return this._runTime}
  }
  
  class CD extends Media {
    constructor (title, artist, songs)
    {
      super(title)
      this._artist = artist
      this._songs = [songs]
    }
    
    get artist() {return this._artist}
    get songs() {return this._songs}
  }
  
  //book added
  const lotrBook = new Book("Lord of the Rings", "J.R.R Tolkein", 500)
  lotrBook.toggleCheckOutStatus()
  lotrBook.addRating(5)
  lotrBook.addRating(8)
  lotrBook.addRating(9)
  lotrBook.addRating(6)
  console.log(lotrBook)
  console.log(lotrBook.getAverageRating())
  
  //movie added
  const brMovie = new Movie("Blade Runner", "Ridley Scott", 220)
  brMovie.toggleCheckOutStatus()
  brMovie.addRating(10)
  brMovie.addRating(10)
  brMovie.addRating(9)
  brMovie.addRating(7)
  console.log(brMovie)
  console.log(brMovie.getAverageRating())
  
  const dbCD = new CD ("Blackstar", "David Bowie", ['Ziggy Stardust', 'Life on Mars'])
  dbCD.toggleCheckOutStatus()
  dbCD.addRating(10)
  dbCD.addRating(10)
  dbCD.addRating(9)
  dbCD.addRating(7)
  console.log(dbCD)
  console.log(dbCD.getAverageRating())
  